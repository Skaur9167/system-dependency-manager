﻿// ******** Group 5 Members********
// * Alabarce Junqueira, Bernardo  *
// * Kaur, Samreen                 *
// * Kiani Far, Ehsan              *
// * Rathnakumar, Vasanth          *
// * Rederburg, Todd               *
// *********************************
using Microsoft.Win32;
using System.Windows;
namespace SystemDependency
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog bDialog = new OpenFileDialog();
            bDialog.Title = "Open Word or Text File";
            bDialog.Filter = "Text Files|*.doc;*.docx;*.txt;*.text";
            bDialog.InitialDirectory = @"C:\";

            if (bDialog.ShowDialog() == true)
            {
                vm.OpenDocument(bDialog.FileName);;
            }
        }
            private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            vm.Dependency();
        }
    }
}