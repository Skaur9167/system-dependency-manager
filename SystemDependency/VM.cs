﻿// ******** Group 5 Members********
// * Alabarce Junqueira, Bernardo  *
// * Kaur, Samreen                 *
// * Kiani Far, Ehsan              *
// * Rathnakumar, Vasanth          *
// * Rederburg, Todd               *
// *********************************
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
namespace SystemDependency
{
    class VM: INotifyPropertyChanged
    {
        public const string DEPEND="DEPEND";
        public const string INSTALL = "INSTALL";
        public const string REMOVE = "REMOVE";
        public const string LIST = "LIST";
        public const string END = "END";
        public String input;
        public String output;
        private string[] ReadFromFile { get; set; }
        public String Input { get => input; set { input = value; NotifyChanged(); } }
        public String Output { get => output; set { output = value; NotifyChanged(); } }
        Dictionary<string, HashSet<string>> componentDependencies = new Dictionary<string, HashSet<string>>();
        Dictionary<string, HashSet<string>> installedComponentDependencies = new Dictionary<string, HashSet<string>>();
        HashSet<string> ExplicitlyInstalledComponentSet = new HashSet<string>();
        public void OpenDocument(string path)
        {
            try
            {
                ReadFromFile = File.ReadAllLines(path);
                foreach(string i in ReadFromFile)
                {
                    Input=input+i+"\n";
                }
            }
            catch
            {
                Input = "Unable to read file";
            }
        }
        public void Dependency()
        {
            componentDependencies = new Dictionary<string, HashSet<string>>();
            installedComponentDependencies = new Dictionary<string, HashSet<string>>();
            ExplicitlyInstalledComponentSet = new HashSet<string>();
            Output = "";
            for (int i = 0; i < ReadFromFile.Length; i++)
            {
                string[] words = ReadFromFile[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    switch (words[0])
                    {
                        case "DEPEND":
                            Depend(words);
                            break;
                        case "INSTALL":
                            Install(words[1]);
                            break;
                        case "REMOVE":
                            Remove(words[1]);
                            break;
                        case "LIST":
                            ListItem();
                            break;
                        case "END":
                            End();
                            break;
                        default:
                            Output =words[0]+ " IS NOT A VALID COMMAND\n";
                            break;
                    }
            }   
        }
        private void End()
        {
            Output=output+"END";
        }
        private void Depend(String[] words)
        {
            string component = words[1];
            Output = output + "DEPEND \t" + component;
            HashSet<string> dependencies = new HashSet<string>();
            for (int i = 2; i < words.Length; i++)
            {
                dependencies.Add(words[i]);
                Output = output + " " + words[i];
            }
            Output = output + "\n";
            componentDependencies.Add(component, dependencies);
        }
        private void Install(string component)
        {
            Output = output + "INSTALL \t" + component+"\n";
            if (ExplicitlyInstalledComponentSet.Contains(component))
            {
                Output = output + " " + component + " is already installed\n";
            }
            else
            {
                ExplicitlyInstalledComponentSet.Add(component);
                InstallByDependency(component, null);
            }
        }
        private void InstallByDependency(string component, string requiredBy)
        {
            if (!installedComponentDependencies.ContainsKey(component))
            {
                if (componentDependencies.ContainsKey(component))
                {
                    componentDependencies.TryGetValue(component, out HashSet<string> dependencies);
                    foreach (string dependency in dependencies)
                    {
                        InstallByDependency(dependency, component);
                        if (!installedComponentDependencies.ContainsKey(dependency))
                        {
                            Output = output + "   Installing " + dependency+"\n";
                        }
                        RecordInstallation(dependency, component);
                    }
                }
               Output = output + "   Installing " + component+"\n";
            }
            RecordInstallation(component, requiredBy);
        }
        public void RecordInstallation(string component, string requiredBy)
        {
            installedComponentDependencies.TryGetValue(component, out HashSet<string> requiredBySet);
            if (requiredBySet == null)
            {
                installedComponentDependencies.Add(component, new HashSet<string>());
            }
            else
            {
                installedComponentDependencies.TryGetValue(component, out HashSet<string> existingRequiredBySet);
                existingRequiredBySet.Add(requiredBy);
            }
        }
        private void Remove(String component)
        {
            Output = output + "REMOVE\t" + component+"\n";
            Boolean isRemoved = TryToRemoveComponent(component, false);
            if (isRemoved)
            {
                ExplicitlyInstalledComponentSet.Remove(component);
                String[] compToBeRemovedList = installedComponentDependencies.Keys.ToArray();
                foreach (String compToBeRemoved in compToBeRemovedList)
                {
                    if (!ExplicitlyInstalledComponentSet.Contains(compToBeRemoved))
                    {
                        TryToRemoveComponent(compToBeRemoved, true);
                    }
                }
            }
        }
        private Boolean TryToRemoveComponent(String component, Boolean isCleanup)
        {
            HashSet<String> requirementBySet;
            installedComponentDependencies.TryGetValue(component, out requirementBySet);
            if (requirementBySet == null)
            {
                Output = output + "   " + component + " is not installed.\n";
                return false;
            }
            if (requirementBySet.Count() > 0)
            {
                if (!isCleanup)
                {
                    Output = output + "   " + component + " is still needed.\n";
                }
                return false;
            }
            else
            {
                installedComponentDependencies.Remove(component);
                Output = output + "   Removing " + component+"\n";
                RemoveReferences(component);
                return true;
            }
        }
        private void RemoveReferences(String reference)
        {
            foreach (string i in installedComponentDependencies.Keys)
            {
                installedComponentDependencies.TryGetValue(i, out HashSet<string> requirementBySet);
                requirementBySet.Remove(reference);
            }
        }
         private void ListItem()
        {
            Output = output + "LIST"+"\n";
            foreach (String i in installedComponentDependencies.Keys)
            {
                Output = output + "   " + i+"\n";
            }
        }
        #region Property Changed
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
